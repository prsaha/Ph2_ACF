set(EMP_FOUND FALSE)

if(${EMP_LOCAL})
    if(EXISTS ${PROJECT_SOURCE_DIR}/extern/emp-toolbox)
        MESSAGE(STATUS "Using local EMP")

        set(EMP_TOP_DIR ${PROJECT_SOURCE_DIR}/extern/emp-toolbox)
        set(EMP_INCLUDE_DIRS ${EMP_TOP_DIR}/core/include ${EMP_TOP_DIR}/logger/include)
        set(EMP_LIB_DIRS ${EMP_TOP_DIR}/core/lib ${EMP_TOP_DIR}/logger/lib)
        file(GLOB_RECURSE EMP_LIBRARIES ${EMP_TOP_DIR}/core/lib/libcactus_emp.so ${EMP_TOP_DIR}/logger/lib/libcactus_emp_logger.so)
        set(EMP_FOUND TRUE)
    endif()
else()
    if(EXISTS /opt/cactus/bin/emp)
        MESSAGE(STATUS "Using EMP RPM")

        set(EMP_INCLUDE_DIRS /opt/cactus/include)
        set(EMP_LIB_DIRS /opt/cactus/lib)
        file(GLOB_RECURSE EMP_LIBRARIES ${EMP_LIB_DIRS}/libcactus*.so)
        set(EMP_FOUND TRUE)
    endif()
endif()

