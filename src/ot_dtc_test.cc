#include <cstring>

#include "tools/BeamTestCheck.h"
#include "tools/CBCPulseShape.h"
#include "tools/CicFEAlignment.h"
#include "HWInterface/D19cDebugFWInterface.h"
#include "tools/LatencyScan.h"
#include "tools/LinkAlignmentOT.h"
#include "tools/LinkTestOT.h"
#include "tools/VTRxController.h"
#include "HWInterface/DTCFastCommandInterface.h"
#ifndef __EMP__
#include "ProductionToolsOT//CicInputTest.h"
#include "ProductionToolsOT//SFPMonitoringTool.h"
//#include "ProductionToolsOT/BackendAlignmentOT.h"
#include "tools/CheckCbcNeighbors.h"
#include "tools/MemoryCheck2S.h"
#include "tools/ShortFinder.h"
#endif
#include "HWInterface/DTCFWInterface.h"
#include "tools/OTCMNoise.h"
#include "tools/OTModuleStartUp.h"
#include "tools/OTTemperature.h"
#include "tools/PSAlignment.h"
#include "tools/PedeNoise.h"
#include "tools/PedestalEqualization.h"
#include "tools/RegisterTester.h"
#include "Utils/Timer.h"
#include "Utils/Utilities.h"
#include "Utils/argvparser.h"
#include "boost/format.hpp"
#ifdef __POWERSUPPLY__
// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#endif

#ifdef __USE_ROOT__
#include "TApplication.h"
#include "TROOT.h"
#endif

#define __NAMEDPIPE__

#ifdef __NAMEDPIPE__
#include "Utils/gui_logger.h"
#endif

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;
INITIALIZE_EASYLOGGINGPP

#define CHIPSLAVE 4

sig_atomic_t killProcess  = 0;
sig_atomic_t runCompleted = 0;

void interruptHandler(int handler) { killProcess = 1; }

void killProcessFunction(Tool* theTool)
{
    while(1)
    {
        usleep(250000);
        if(killProcess || runCompleted) break;
    }
    if(killProcess)
    {
        theTool->Destroy();
        abort();
    }
}

uint16_t returnRunNumber(std::string cFileName)
{
    std::string   cLine;
    int           cRunNumber = -1;
    std::ifstream cStream(cFileName);
    if(cStream.is_open())
    {
        while(std::getline(cStream, cLine))
        {
            std::istringstream cIStream(cLine);
            cIStream >> cRunNumber;
            // LOG(INFO) << BOLDMAGENTA << cRunNumber << RESET;
        }
    }
    return (uint16_t)(cRunNumber + 1);
}

std::vector<uint8_t> getArgs(std::string pArgsStr)
{
    std::vector<uint8_t> cSides;
    std::stringstream    cArgsSS(pArgsStr);
    int                  i;
    while(cArgsSS >> i)
    {
        cSides.push_back(i);
        if(cArgsSS.peek() == ',') cArgsSS.ignore();
    };
    return cSides;
}

int main(int argc, char* argv[])
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("OT DTC Test Application");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("file", "Hw Description File . Default value: settings/OT_DTC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");
    cmd.defineOption("connectionsFile", "Connections description file File . Default value: settings/Commission_2CBC.xml", ArgvParser::OptionRequiresValue);

    cmd.defineOption("sendStubOnPS", "Send a stub on chip ID X: (non physical ID means header only)", ArgvParser::OptionRequiresValue);
    cmd.defineOption("sendStubOn2S", "Send a stub on chip ID X: (non physical ID means header only)", ArgvParser::OptionRequiresValue);

    cmd.defineOption("setAddress", "Set address value: default 20 (use with --sendStubOn)", ArgvParser::OptionRequiresValue);
    cmd.defineOption("setBend", "Set bend value: default 0 (use with --sendStubOn)", ArgvParser::OptionRequiresValue);

    cmd.defineOption("setDLL", "Set CBC dll value", ArgvParser::OptionRequiresValue);
    cmd.defineOption("setThreshold", "Set VCTH threshold value", ArgvParser::OptionRequiresValue);
    cmd.defineOption("setModule", "Set module for setting threshold/dll", ArgvParser::OptionRequiresValue);
    cmd.defineOption("setChip", "Set module for setting threshold/dll", ArgvParser::OptionRequiresValue);

    cmd.defineOption("enableCicPattern", "Enable CIC pattern mode");
    cmd.defineOption("disableCicPattern", "Disable CIC pattern mode");



    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile          = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/OT_DTC.xml";

    unsigned cFixedStubPS        = (cmd.foundOption("sendStubOnPS")) ? convertAnyInt(cmd.optionValue("sendStubOnPS").c_str()) : -1;
    unsigned cFixedStub2S        = (cmd.foundOption("sendStubOn2S")) ? convertAnyInt(cmd.optionValue("sendStubOn2S").c_str()) : -1;
    unsigned cFixedAddress       = (cmd.foundOption("setAddress")) ? convertAnyInt(cmd.optionValue("setAddress").c_str()): 20;
    unsigned cFixedBend          = (cmd.foundOption("setBend")) ? convertAnyInt(cmd.optionValue("setBend").c_str()) : 0;
    unsigned cThreshold          = (cmd.foundOption("setThreshold")) ? convertAnyInt(cmd.optionValue("setThreshold").c_str()) : -1;
    unsigned cDll                = (cmd.foundOption("setDLL")) ? convertAnyInt(cmd.optionValue("setDLL").c_str()) : -1;
    unsigned cModuleSelect       = (cmd.foundOption("setModule")) ? convertAnyInt(cmd.optionValue("setModule").c_str()) : -1;
    unsigned cChipSelect         = (cmd.foundOption("setChip")) ? convertAnyInt(cmd.optionValue("setChip").c_str()) : -1;

    std::string cModuleId        = (cmd.foundOption("moduleId")) ? cmd.optionValue("moduleId") : "ModuleOT";
    std::string cDirectory       = (cmd.foundOption("output")) ? cmd.optionValue("output") : "Results/";

    uint16_t          cRunNumber = 666;
    std::stringstream cOutput;

    std::ofstream cRunLog;
    cRunNumber = returnRunNumber("RunNumbers.dat");
    cRunLog.open("RunNumbers.dat", std::fstream::app);
    cRunLog << cRunNumber << "\n";
    cRunLog.close();
    LOG(INFO) << BOLDBLUE << "Run number is " << +cRunNumber << RESET;

    cOutput << "OT_ModuleTest_" << cModuleId.c_str() << "_Run" << cRunNumber;
    cDirectory += cOutput.str();
    LOG(INFO) << BOLDYELLOW << cDirectory << RESET;

    std::string cResultfile = "Hybrid";
    Timer       t;
    Timer       cGlobalTimer;
    cGlobalTimer.start();

    std::stringstream outp;
    Tool              cTool;

    std::thread softKillThread(killProcessFunction, &cTool);
    softKillThread.detach();

    struct sigaction act;
    act.sa_handler = interruptHandler;
    sigaction(SIGINT, &act, NULL);

    cTool.CreateResultDirectory(cDirectory);
    cTool.InitResultFile(cResultfile);
    cTool.InitializeHw(cHWFile, outp);
    
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();

    BeBoardInterface* cBeBoardInterface = cTool.fBeBoardInterface;


    for (auto cBoard : *cTool.fDetectorContainer) {

      cBeBoardInterface->setBoard(cBoard->getId());
      #ifdef __EMP__
        DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());

        DTCFastCommandInterface *fDtcFastCmdInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());
        fDtcFastCmdInterface->InitialiseTCDS(cBoard);
        fDtcFastCmdInterface->SendGlobalReSync(0);
      #endif

    }



    // ***************
    // send PS stub patterns
    // ***************

    if (cFixedStubPS != (unsigned)-1) {

      for (auto cBoard : *cTool.fDetectorContainer) {

        cBeBoardInterface->setBoard(cBoard->getId());

        uint8_t cRow        = 5; // random pixel to activate -- could be configurable
        uint8_t cCol        = cFixedAddress;
	      LOG(INFO) << "What?" << RESET;
        for (auto cOpticalReadout: *cBoard) {
	        LOG(INFO) << "Board" << RESET;
          for (auto cHybrid: *cOpticalReadout) {

            LOG(INFO) << BOLDBLUE << "Configuring hybrid id = " << +cHybrid->getId() << RESET;

            for (auto cChip: *cHybrid) {
	          LOG(INFO) << "chip loop" << RESET;
              auto fReadoutChipInterface = static_cast<PSInterface*>(cTool.fReadoutChipInterface);

              if (cChip->getId()%8 == cFixedStubPS) {

                if (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) {
                  fReadoutChipInterface->WriteChipReg(cChip, "Mask_ALL", 0xff);
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                  fReadoutChipInterface->WriteChipReg(cChip, "ECM", 0x80); //turn it to normal mode, pixel-pixel mode is being used for debugging
                  fReadoutChipInterface->WriteChipReg(cChip, "ReadoutMode", 0x0);
                  uint32_t cGpix = static_cast<MPA*>(cChip)->PNglobal(std::pair<uint32_t, uint32_t>(cRow, cCol));
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_P" + std::to_string(cGpix), 0x37);
                  fReadoutChipInterface->WriteChipReg(cChip, "DigitalSync_P" + std::to_string(cGpix), 0x01); // enable 1 pix
                  LOG(INFO) << BOLDMAGENTA << "Enabling stubs on address " << cFixedAddress << " of MPA chip " << cFixedStubPS << " with bend value " << cFixedBend << RESET;
                }

                if (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) {
                  fReadoutChipInterface->WriteChipReg(cChip, "mask_strip", 0xff);
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                  fReadoutChipInterface->WriteChipReg(cChip, "DigCalibPattern_L_ALL", 0x01);
                  fReadoutChipInterface->WriteChipReg(cChip, "DigCalibPattern_H_ALL", 0x01);
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_S" + std::to_string(cCol), 0x9);
                  LOG(INFO) << BOLDMAGENTA << "Enabling stubs on address " << cFixedAddress << " of SSA chip " << cFixedStubPS << " with bend value " << cFixedBend << RESET;
                }

              } else {

                if (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) {

                  fReadoutChipInterface->WriteChipReg(cChip, "Mask_ALL", 0xff);
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                  fReadoutChipInterface->WriteChipReg(cChip, "ReadoutMode", 0x0);

                  LOG(INFO) << BOLDMAGENTA << "Disabling stubs on MPA chip " << +cChip->getId()%8 << RESET;
                }

                if (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) {
                  LOG(INFO) << BOLDMAGENTA << "Disabling stubs on SSA chip " << +cChip->getId()%8<< RESET;
                  fReadoutChipInterface->WriteChipReg(cChip, "mask_strip", 0xff);
                  fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                  fReadoutChipInterface->WriteChipReg(cChip, "ReadoutMode", 0x0);
                }
              }
            }
          }
        }

        #ifdef __EMP__
          DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());
          DTCFastCommandInterface *fDtcFastCmdInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());

          FastCommand cmd;
          cmd.cal_pulse_en = true;
          cmd.bx_wait = 7;
          std::vector<FastCommand> fastCmd{cmd};

          fDtcFastCmdInterface->SendFastCommands(fastCmd, -1); // infinite repeat
        #endif

        //============TEST TI READOUT REGISTERS===========
        for (auto cOpticalReadout: *cBoard) {
          LOG(INFO) << "Board" << RESET;
          for (auto cHybrid: *cOpticalReadout) {

            LOG(INFO) << BOLDBLUE << "Reading hybrid id = " << +cHybrid->getId() << RESET;

            for (auto cChip: *cHybrid) {
              LOG(INFO) << "chip loop" << RESET;
              auto fReadoutChipInterface = static_cast<PSInterface*>(cTool.fReadoutChipInterface);
                if (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) {

                  LOG(INFO) << BOLDMAGENTA << "Reading registers of MPA" << +cChip->getId()%8 << RESET;

              	  unsigned ECM_reg = fReadoutChipInterface->ReadChipReg(cChip, "ECM");
              	  LOG(INFO) << "Reading ECM reg: "<< ECM_reg << RESET;

                  unsigned ENFLAGS_P1_reg = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS_P1");
                  LOG(INFO) << "Reading ENFLAGS_P1 reg: "<< ENFLAGS_P1_reg << RESET;

                  unsigned ENFLAGS_P20_reg = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS_P19");
                  LOG(INFO) << "Reading ENFLAGS_P20 reg: "<< ENFLAGS_P20_reg << RESET;

                  unsigned DigPattern_P1_reg = fReadoutChipInterface->ReadChipReg(cChip, "DigPattern_P1");
                  LOG(INFO) << "Reading DigPattern_P1 reg: "<< DigPattern_P1_reg << RESET;

		              unsigned DigPattern_P2_reg = fReadoutChipInterface->ReadChipReg(cChip, "DigPattern_P2");
                  LOG(INFO) << "Reading DigPattern_P2 reg: "<< DigPattern_P2_reg << RESET;

                  unsigned Mask_reg = fReadoutChipInterface->ReadChipReg(cChip, "Mask");
                  LOG(INFO) << "Reading Mask reg: "<< Mask_reg << RESET;

                  unsigned Threshold_reg = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
                  LOG(INFO) << "Reading Threshold reg: "<< Threshold_reg << RESET;

                  unsigned ReadoutMode_reg = fReadoutChipInterface->ReadChipReg(cChip, "ReadoutMode");
                  LOG(INFO) << "ReadoutMode reg: "<< ReadoutMode_reg << RESET;

                  unsigned LFSR_data_reg = fReadoutChipInterface->ReadChipReg(cChip, "LFSR_data");
                  LOG(INFO) << "LFSR_data reg: "<< LFSR_data_reg << RESET;

                }

                if (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) {

                  unsigned mask_strip_reg = fReadoutChipInterface->ReadChipReg(cChip, "mask_strip");
                  LOG(INFO) << "Reading mask_strip reg: "<< mask_strip_reg << RESET;

                  unsigned ENFLAGS_S1_reg = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS_S1");
                  LOG(INFO) << "Reading ENFLAGS_S1 reg: "<< ENFLAGS_S1_reg << RESET;

                  unsigned ENFLAGS_S20_reg = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS_S20");
                  LOG(INFO) << "Reading ENFLAGS_S20 reg: "<< ENFLAGS_S20_reg << RESET;

                  unsigned ENFLAGS_S40_reg = fReadoutChipInterface->ReadChipReg(cChip, "ENFLAGS_S40");
                  LOG(INFO) << "Reading ENFLAGS_S40 reg: "<< ENFLAGS_S40_reg << RESET;

                  unsigned StripControl2_S1_reg = fReadoutChipInterface->ReadChipReg(cChip, "StripControl2_S1");
                  LOG(INFO) << "Reading StripControl2_S1 reg: "<< StripControl2_S1_reg << RESET;

                  unsigned StripControl1_S1_reg = fReadoutChipInterface->ReadChipReg(cChip, "StripControl1_S1");
                  LOG(INFO) << "Reading StripControl1_S1 reg: "<< StripControl1_S1_reg << RESET;

                  unsigned OutPattern0_reg = fReadoutChipInterface->ReadChipReg(cChip, "OutPattern0");
                  LOG(INFO) << "Reading OutPattern0 reg: "<< OutPattern0_reg << RESET;

                  unsigned control_1_reg = fReadoutChipInterface->ReadChipReg(cChip, "control_1");
                  LOG(INFO) << "Reading control_1 reg: "<< control_1_reg << RESET;

                  unsigned Threshold_reg = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
                  LOG(INFO) << "Reading Threshold reg: "<< Threshold_reg << RESET;

                  unsigned ReadoutMode_reg = fReadoutChipInterface->ReadChipReg(cChip, "ReadoutMode");
                  LOG(INFO) << "ReadoutMode reg: "<< ReadoutMode_reg << RESET;

                }
            }
          }
        }
//==============================


      }
    }


    // ***************
    // send 2S stub patterns
    // ***************

    if (cFixedStub2S != (unsigned)-1) {

      for (auto cBoard : *cTool.fDetectorContainer) {

        for (auto cOpticalGroup : *cBoard) {

          for (auto cHybrid : *cOpticalGroup) {

            LOG(INFO) << BOLDBLUE << "Configuring hybrid id = " << +cHybrid->getId() << RESET;
            for (auto cChip: *cHybrid) {

              auto cReadoutChipInterface = static_cast<CbcInterface*>(cTool.fReadoutChipInterface);
              auto cReadoutChip          = static_cast<ReadoutChip*>(cChip);

              if(cReadoutChip->getId() == cFixedStub2S) {

                // one stub with bendcode 0
                cReadoutChipInterface->injectStubs(cReadoutChip, {(unsigned char) cFixedAddress}, {(unsigned char)cFixedBend}, true);

                // switch off HitOr
                cReadoutChipInterface->WriteChipReg(cReadoutChip, "HitOr", 0);

                // enable stub logic
                cReadoutChipInterface->selectLogicMode(cReadoutChip, "Sampled", true, true);

                // set pT cut to maximum
                cReadoutChipInterface->WriteChipReg(cReadoutChip, "PtCut", 14);
                cReadoutChipInterface->WriteChipReg(cReadoutChip, "TestPulse", 0);

                LOG(INFO) << BOLDMAGENTA << "Enabling stubs on address " << cFixedAddress << " of chip " << cFixedStub2S << " with bend value " << cFixedBend << RESET;

              } else {

                cReadoutChipInterface->enableHipSuppression(cReadoutChip, false, true, 0); // chip, for hits, for stubs, #clock cycles

                LOG(INFO) << BOLDMAGENTA << "Disabling stubs on chip " << +cReadoutChip->getId() << RESET;
              }

            }
          }
        }
      }
    }


    // ***************
    // set thresholds
    // ***************

    if (cThreshold != (unsigned)-1) {

      for (auto cBoard : *cTool.fDetectorContainer) {

        for (auto cOpticalGroup : *cBoard) {

          if ((cOpticalGroup->getId()==(cModuleSelect-1))||(cModuleSelect == (unsigned)-1)) {

            for (auto cHybrid : *cOpticalGroup) {

              LOG(INFO) << BOLDBLUE << "Configuring hybrid id = " << +cHybrid->getId() << RESET;
              for (auto cChip: *cHybrid) {

                auto cReadoutChipInterface = static_cast<CbcInterface*>(cTool.fReadoutChipInterface);
                auto cReadoutChip          = static_cast<ReadoutChip*>(cChip);
                if((cChip->getId() == (cChipSelect)) || (cChipSelect == (unsigned)-1)) {
                  cReadoutChipInterface->WriteChipReg(cReadoutChip, "VCth", cThreshold);
                  LOG(INFO) << BOLDBLUE << "Setting chip id = " << +cChip->getId() << " threshold to " << cThreshold << RESET;
                }
              }
            }
          }
        }
      
      cBeBoardInterface->setBoard(cBoard->getId());
      #ifdef __EMP__
        DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());

        DTCFastCommandInterface *fDtcFastCmdInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());
        fDtcFastCmdInterface->SendGlobalReSync(0);
        //fDtcFastCmdInterface->SendGlobalRepetitiveL1A(5);
      #endif
      }
    }


    // ***************
    // set dll offsets
    // ***************

    if (cDll != (unsigned)-1) {

      for (auto cBoard : *cTool.fDetectorContainer) {

        cBeBoardInterface->setBoard(cBoard->getId());
        #ifdef __EMP__
          DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());
        #endif

        for (auto cOpticalGroup : *cBoard) {

          if ((cOpticalGroup->getId()==(cModuleSelect-1))||(cModuleSelect == (unsigned)-1)) {

            for (auto cHybrid : *cOpticalGroup) {

              LOG(INFO) << BOLDBLUE << "Configuring hybrid id = " << +cHybrid->getId() << RESET;
              for (auto cChip: *cHybrid) {

                auto cReadoutChipInterface = static_cast<CbcInterface*>(cTool.fReadoutChipInterface);
                auto cReadoutChip          = static_cast<ReadoutChip*>(cChip);

                cReadoutChipInterface->WriteChipReg(cReadoutChip, "DLL", cDll);
                LOG(INFO) << BOLDBLUE << "Setting chip id = " << +cChip->getId() << " dll to " << cDll << RESET;

              }
            }

            LOG(INFO) << BOLDBLUE << "Sending resync... " << RESET;
            #ifdef __EMP__
              cDtcFwInterface->ChipReSync();
            #endif

          }
        }
      }
    }

    // ***************
    // set cic output pattern
    // ***************
    if ( cmd.foundOption("enableCicPattern") || cmd.foundOption("disableCicPattern") ) {

      for (auto cBoard : *cTool.fDetectorContainer) {

        for (auto cOpticalGroup : *cBoard) {

          for (auto cHybrid : *cOpticalGroup) {

            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            auto cCicInterface = static_cast<CicInterface*>(cTool.fCicInterface);

            if (cmd.foundOption("enableCicPattern")) cCicInterface->SelectOutput(cCic, true);
            if (cmd.foundOption("disableCicPattern")) cCicInterface->SelectOutput(cCic, false);

          }
        }
      }
    }


    cTool.SaveResults();
    cTool.Destroy();
    signal(SIGINT, SIG_DFL);
    runCompleted = 1;
    cGlobalTimer.stop();
    cGlobalTimer.show("Total execution time: ");

    return 0;
}
